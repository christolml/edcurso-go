package main

import "fmt"

func main() {

	var a int
	var b int8
	n := "Christopher"
	p := "Velázquez"

	a = 1245124
	b = 5

	/*go es muy estricto de acuerdo a los tipos de datos
	por eso es el casting con el cual se hace un cambio temporal de una variable   */
	//casting int(b) ya que b es de tipo int8 con el casting se hace un cambio temporal
	c := a + int(b)

	fmt.Println(c)
	/*con Printf permite hacer un formate de algo que se hará, en el ejemp de abajo
	en el %s estoy diciendo que es un campo de tipo string que se va a mostrar, ya solo al final con una coma
	agrego mi variable de tipo string  */

	//con %s imprimo string   %d imprimo un número
	fmt.Printf("Hola %s %s cómo estás?\n ", n, p)
	//con %T nos permite imprimir el tipo de datos que es una variable
	fmt.Printf("La variable a es de tipo: %T\n La variable b es de tipo: %T\n La variable c es de tipo: %T", a, b, c)

}
