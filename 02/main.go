package main

import "fmt"

//clase sobre variables y constantes

func main() {
	///////////////////////////    Variables       ///////////////////////////////////////////////////////////////

	/*
		//var nombre, apellido string
		nombre, apellido := "Christopher", "Velázquez" //los := sirve para que go elige el tipo de dato
		//apellido := "Velázquez"

		//cuando uso := con una var que ya la declare arriba debo
		//crear una nueva var aqui para poder usar el := con estas nuevas variables
		nombre, edad := "Emmanuelle", 20

		fmt.Println(nombre, apellido, edad)
	*/

	///////////////////////////    Constantes       ///////////////////////////////////////////////////////////////

	/*cuando se crea una constante se le debe asignar el valor ahí mismo, después no se puede,
	también sabe de que tipo es la constante */
	const nombre = "Christopher"
	// nombre = "Emmanuelle"    no se le puede volver asignar un valor a mi constante

	fmt.Println(nombre)

}
