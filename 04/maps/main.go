package main

import "fmt"

func main() {
	//------------MAPS--------------
	// formas personalizadas , datos asociativo
	//sintaxis para crear un mapa    make(map["el tipo que sera mi llave"]"tipo de datos que va almacenar")
	// make(map[key-type]val-type)

	/*
		idiomas := make(map[string]string, 0)
		idiomas["es"] = "Espanol"
		idiomas["en"] = "Ingles"
		idiomas["fr"] = "Frances"

		fmt.Println(idiomas)
	*/

	// ---------------DECLARAR MAPS ASIGNADO DIRECTAMENTE EL VALOR

	idiomas := map[string]string{"es": "Espanol", "en": "Ingles", "fr": "Frances", "pt": "Portugues"}
	fmt.Println(idiomas)
	// Espacificar uno que quiero imprimir, acceder a los valores
	fmt.Println(idiomas["es"])

	//Borrar una posicion del mapa
	delete(idiomas, "en")
	fmt.Println(idiomas)

	// si mando a imprimir algo de un mapa que no exista lo que se va a imprimir sera nada un espacio en blanco
	// ya que como no existe el valor me devuelve un valor 0 y el valor 0 de un mapa de tipo string es vacio y por
	// eso devuelve vacio
	fmt.Println(idiomas["pt"])

	// se crea la variable idioma para colocarle el valor de la posicion de mi map
	// este tambien nos devuelve un boolean y por eso se usa la variable ok
	// el mapa nos devuelve dos valores por eso usamos dos variables, en una es para
	// el contenido y en la otra para saber si nos devolvio true o false el mapa
	// "ok{...." significa que la sentencia es true

	if idioma, ok := idiomas["pt"]; ok {
		fmt.Println("La posicion si existe y vale", idioma)
	} else {
		fmt.Println("La posicion no existe")
	}

	numeros := map[int]string{1: "Uno es un num", 2: "Dos es un num", 3: "3 es otro num", -4: "este num es negativo"}

	fmt.Println(numeros)

}
