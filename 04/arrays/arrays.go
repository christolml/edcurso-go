package main

import "fmt"

func main() {

	// en go los valores deben ser del mismo tipo
	// sintaxis, var name[#]tipo del array
	var nombres [3]string

	nombres[0] = "Christopher"
	nombres[1] = "Esmeralda"
	nombres[2] = "Towi"
	// para imprimir lo que hay en un array no es necesario recorrerlo, solo con mandar a llamar a mi array
	fmt.Println(nombres)

	nombres2 := [3]string{"Christopher", "Esmeralda", "Towi"}
	// para imprimir un solo dato del array solo hay que especificar que espacio se requiere mostrar
	fmt.Println(nombres2[1])
	// para calcular el tamaño de nuestro arreglo usamos len
	size := len(nombres2)
	fmt.Println("El tamaño del arreglo es de: ", size)
	// para traer mas de un elementos de un array
	fmt.Println(nombres2[0:2])
	fmt.Println(nombres2[1:2])

	// el valor cero es el tipo de datos que se esta manejando el array
	var ejemplo [3]string
	var ejemplo2 [3]int
	var ejemplo3 [3]bool
	// espacios en blanco quiere decir que el tipo de datos es de tipo string
	fmt.Println(ejemplo)
	fmt.Println(ejemplo2)
	fmt.Println(ejemplo3)
}
