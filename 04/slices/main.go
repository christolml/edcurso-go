package main

import "fmt"

func main() {

	//-------------SLICES----------- representa una secuencia de tamano variable de
	// elementos del mismo tipo, tienen un apuntador a un arrays
	// 1- Apuntador a un array
	// 2- Tamano
	// 3- Capacidad
	// 1 2
	// 3 6 => 6   7 14

	var nombres []string //no se ocupa especificar el tamano diferencia con un array, este es un slice
	//para poder agregar contenido a mi slice ocupo de una funcion preconstruida de go "append"
	//recibe dos parametros, al slice que se le quiere meter inf, y la informacion
	// con len me dice su tamaño y con cap me dice su capacidad de mi slice

	fmt.Printf("El tamaño del slice es: %d y su capacidad es: %d\n", len(nombres), cap(nombres))
	nombres = append(nombres, "Christopher")
	fmt.Printf("El tamaño del slice es: %d y su capacidad es: %d\n", len(nombres), cap(nombres))
	nombres = append(nombres, "Dana")
	fmt.Printf("El tamaño del slice es: %d y su capacidad es: %d\n", len(nombres), cap(nombres))
	nombres = append(nombres, "Luis")
	fmt.Printf("El tamaño del slice es: %d y su capacidad es: %d\n", len(nombres), cap(nombres))

	// asignarle un valor a un espacio donde ya tenia un dato almacenado
	nombres[2] = "otro nombre"
	fmt.Println(nombres)

	// forma formal de como crear un slice con make
	//sintaxis    make(tipo de dato del slice, tamano inicial, capacidad inicial)
	// nombres2 := make([]string, 0)

	//asignar valores directos, sin append y otra forma de crear un slice
	nombres3 := []string{
		"Christo",
		"Miguel",
		"Rosalinda",
		"Luis",
		"Dana",
	}
	fmt.Println(nombres3)
}
