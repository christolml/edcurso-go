package main

import (
	"fmt"
	"time"
)

/* sin la Go Routine MostrarNumeros() sería una tarea bloqueante, ya que si no se ha finalizado esa no se puede seguir con lo que va después
/Go Routine, poniendo go antes de mi funcion (que es una tarea) estoy indicando que esta es indipendiente de las demas tareas que se ejecutan después
si se escribe durante se esten mostrando los números y presiono enter me ejecuta "fmt.Println("Digitaste", h)" y como esa es la ultima tarea de la función main
no le interesa si go MostrarNumeros() acabo o no

*/
func main() {
	var h string
	go MostrarNumeros()
	fmt.Print("Digita algo: ")
	fmt.Scan(&h)
	fmt.Println("Digitaste", h)
}

func MostrarNumeros() {
	for i := 1; i <= 10; i++ {
		fmt.Println(i)
		time.Sleep(time.Second)
	}
}
