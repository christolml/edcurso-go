package main

import (
	"fmt"
)

// ----------------- CANALES   nos permite que nuestras go routines se esten comunicando entre ellas(se esten pasando informacion)
/*
los canales no pueden tener mas de un valor  a la vez, cuando una go routine esta lista para enviar datos
por un canal la otra go routine debe estar lista para recibir el dato, cuando la segunda go routine la recibe esta
se pone a trabajar con el dato mientras que la primer go routine debe de esperar con el dato nuevo hasta que la segunda
este lista para recibir
*/

func main() {
	bodegaOrigen := []string{"php", "javascript", "html", "css", "java", "base de datos", "git"}
	bodegaDestino := []string{}

	// se estan creando los canales
	fotocopiadora := make(chan string)
	fotocopiado := make(chan string)

	// se cargan los libros al canal fotocopiadora
	go func() {
		for _, libro := range bodegaOrigen {
			fotocopiadora <- libro
		}
	}()

	// Se deja en la bodega destino
	go func() {

		for {
			// se entrega el contenido de la fotocopiadora a la variabe libro (cada vez que fotocopiadora reciba un valor)
			libro := <-fotocopiadora

			// a fotocopiado se le envia lo que hay en libro
			fotocopiado <- libro

			// Agregar el libro a la bodega destino
			bodegaDestino = append(bodegaDestino, libro)

			if len(bodegaOrigen) == len(bodegaDestino) {
				// Cerrar el canal fotocopiado
				close(fotocopiado)
			}
		}
	}()

	fmt.Println("** Listado de libros fotocopiados **")
	// se esta descargando los valores que esta recibiendo el canal fotocopiado
	// cuando obtenemos el valor de un canal obtenemos dos cosas, el valor en si y a la vez un boolean que nos dice si esta
	// abierto o cerrado
	for {
		if libro, ok := <-fotocopiado; ok {
			fmt.Println(libro)
		} else {
			break
		}
	}
}
