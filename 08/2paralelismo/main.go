package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	//le indicamos con cuantos procesadores va a trabajar
	runtime.GOMAXPROCS(4)
	// Esta herramienta (waitgroup) lo podemos implementar para saber cuantas go routines se han realizado
	var wg sync.WaitGroup
	numbers := []uint32{12343, 253, 5425324, 432456742, 2345433446, 34311896, 867864456, 42435454, 4523567, 8754257, 9987645, 234354657}
	// se le indica cuantas go routines se van a lanzar, le indicamos a waitgroup que nos espere hasta que esas tareas sean ejecutadas
	wg.Add(len(numbers))
	fmt.Println("Comienza el proceso...")
	for _, value := range numbers {
		go func(a uint32) { // se esta creando una go routine con una func anonima
			defer wg.Done() // pase lo que pase en esta go routine se va a ejecutar esto con defer, le estamos quitando las tareas establecidas en Add
			fmt.Println(a, EsPrimo(a))
		}(value)
		// fmt.Println(value, EsPrimo(value))
	}
	wg.Wait() // le estamos diciendo al main que se espere hasta que termine mi wg (waitgroup)
	fmt.Println("Proceso Terminado")
}

func EsPrimo(a uint32) bool {
	c := 0
	var i uint32
	for i = 1; i <= a; i++ {
		if a%i == 0 {
			c++
		}
	}
	if c == 2 {
		return true
	}
	return false

}
