package main

import "fmt"

func main() {
	var miEntero int = 42
	var miPuntero *int = &miEntero

	fmt.Println("Este es tu entero al principio, pequeño padawan:", miEntero)
	fmt.Println("Este es tu puntero al principio, pequeño padawan:", miPuntero)
	fmt.Println("Este es el entero al que apunta tu puntero al principio, pequeño padawan:", *miPuntero)

	fmt.Println("\nTu entero no va a cambiar de momento, pequeño padawan.\n")

	cambiarEnteroPasado(miEntero)

	fmt.Println("Este sigue siendo tu entero, pequeño padawan:", miEntero)
	fmt.Println("Este sigue siendo tu puntero, pequeño padawan:", miPuntero)
	fmt.Println("Este sigue siendo el entero al que apunta tu puntero, pequeño padawan:", *miPuntero)

	fmt.Println("\nTu entero ahora sí va a cambiar, pequeño padawan.\n")

	cambiaEnteroApuntado(miPuntero)

	fmt.Println("Finalmente, tu entero es:", miEntero)
	fmt.Println("Finalmente, tu puntero es:", miPuntero)
	fmt.Println("Finalmente, el entero al que apunta tu puntero es:", *miPuntero)
}

func cambiarEnteroPasado(i int) {
	fmt.Println("He recibido el valor", i)
	i = i + 17
	fmt.Println("He transformado el valor en", i)
}

func cambiaEnteroApuntado(ptrI *int) {
	fmt.Println("He recibido el puntero", ptrI)
	fmt.Println("El valor apuntado es", *ptrI)
	*ptrI = *ptrI + 17
	fmt.Println("He transformado el puntero en", ptrI)
	fmt.Println("He transformado el valor apuntado en", *ptrI)
}
