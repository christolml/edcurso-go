package main

import "fmt"

func main() {

	// con break podemos detener el ciclo, y lo que esta depués del break no se ejecuta
	// con continue es como decir que salte esa iteración
	for i := 0; i < 5; i++ {
		if i == 3 {
			continue
		}
		fmt.Println(i)
	}

	for i := 0; i < 5; i++ {
		if i == 2 {
			break
		}
		fmt.Println(i)
	}

	matriz := [3][3]int{}
	valor := 0

	// se esta llenando la matriz por medio de los for de abajo
	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			valor++
			matriz[i][j] = valor
		}
	}

	fmt.Println(matriz)

	for fila := 0; fila < 3; fila++ {
		for columna := 0; columna < 3; columna++ {
			fmt.Println(matriz[fila][columna])
		}
	}

	//-----------------------------WHILE-------------------------------
	// en go no existe while y para ello se usa for con solamente la condición

	a := 5

	for a > 0 {
		a--
		fmt.Println(a)
	}

	//----------------------FOR infinito------------
	// este for es infitio, no tiene condición que lo detenga
	// se usa para procesos en los que se requiere escuchar permanentemente
	// como sockets, conexiones, etc
	// for {
	// 	fmt.Println("Hola")
	// }

	//-----------------FOR RANGE-------------
	/*
	   La forma de rango del bucle for itera sobre un sector o mapa.
	   Cuando se extiende sobre un sector, se devuelven dos valores para cada iteración.
	    El primero es el índice, y el segundo es una copia del elemento en ese índice.
	*/
	numeros := []string{"cero", "uno", "dos", "tres"}
	nombres := map[string]string{"es": "España", "co": "Colombia", "br": "Brasil"}
	frase := "Hola mundo"
	// range nos devuelve dos valores, el indice y el valor
	for i, v := range numeros {
		fmt.Println(i, v)
	}

	for i, v := range nombres {
		fmt.Println(i, v)
	}

	// en este caso se uso un casting ya que go interpreta a mi variable letra como un numero (un bite)
	for posicion, letra := range frase {
		fmt.Println(posicion, string(letra))
	}

	// asignando los valores de mi objeto en el for range
	// con _ (guión bajo) digo que me ignore el indice
	for _, letra := range "Curso Go desde Cero" {
		fmt.Println(string(letra))
	}

	for _, entero := range []int{15, 36, 24, 85} {
		fmt.Println(entero)
	}

}
