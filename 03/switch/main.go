package main

import "fmt"

// func main() {

// 	var a uint
// 	a = 4

// 	// no es obligatoria la palabra break al final de cada case, default si se mantiene
// 	switch a {
// 	case 1:
// 		fmt.Println("Lunes")
// 	case 2:
// 		fmt.Println("Martes")
// 	case 3:
// 		fmt.Println("Miercoles")
// 	case 4:
// 		fmt.Println("Jueves")
// 	case 5:
// 		fmt.Println("Viernes")
// 	case 6:
// 		fmt.Println("Sabado")
// 	case 7:
// 		fmt.Println("Domingo")
// 	default:
// 		fmt.Println("No es un día de la semana")

// 	}

// }

func main() {

	// var a uint8
	// a=5
	// a = 40

	// fallthrough es como decir un por favor sigue evaluando en los case del switch
	// switch a {
	// case 1:
	// 	fallthrough
	// case 2:
	// 	fallthrough
	// case 3:
	// 	fallthrough
	// case 4:
	// 	fallthrough
	// case 5:
	// 	fmt.Println("Estás entre semana")
	// case 6:
	// 	fallthrough
	// case 7:
	// 	fmt.Println("Estás en fin de semana")
	// default:
	// 	fmt.Println("No es un día valido")
	// }

	switch a := 7; {
	case a >= 0 && a <= 5:
		fmt.Println("Estas entre semana")

	case a >= 6 && a <= 7:
		fmt.Println("Estas en fin de semana")
	default:
		fmt.Println("No es un día valido")
	}
}
