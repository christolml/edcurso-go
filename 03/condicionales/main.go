package main

import "fmt"

func main() {

	// isValid := true
	// edad := 10

	//en go no se necesita () en if
	// if 5 > 10 {
	// 	fmt.Println("Esta wea se ejecuta cuando es true :3")
	// }

	//este es como decir si esto no es "la condición dentro del parentesis"
	// if !(5 < 10) {
	// 	fmt.Println("Esta wea se ejecuta cuando es true :3")
	// }

	//como mi variable esta en true es por eso que si jala el programa
	// if isValid {
	// 	fmt.Println("Esta wea se ejecuta cuando es true :3")
	// 	fmt.Println("La variable esta en ", isValid)
	// 	if 5 < 10 {
	// 		fmt.Println("5 es menor de 10")
	// 	}
	// } else {
	// 	fmt.Println("Esto esta en el bloque false")
	// 	fmt.Println("La variable esta en ", isValid)
	// }

	if edad, nombre := 21, "Christo"; edad < 14 {
		fmt.Println(nombre, " es un niño u.u ")
	} else if edad < 18 {
		fmt.Println(nombre, " es un adolescente")
	} else if edad < 30 {
		fmt.Println(nombre, " es un adulto")
	}

	fmt.Println("Fin del programa")
}
