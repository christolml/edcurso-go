package main

import "fmt"

// la estructura de abajo se puede asimilar a las clases en la orientacion oriendtada a objetos en otros lenguajes
type Persona struct {
	nombre string
	edad   int8
}

// creo un nuevo tipo de dato
type Numero int

// en la firma de la funcion de abajo tiene de especial que antes del nombre de la funcion saludar especificamos
// que tipo de dato se le va asignar a ese metodo, a mi metodo saludar le estoy especificando a mi estructura Persona
func (person Persona) saludar() {
	fmt.Println("Holi soy una persona")
}

// en go no es necesario crear estructura para asignarlas a un metodo, se puede asignar cualquier tipo de dato se puede asignarle un metodo
func (num Numero) presentarse() {
	fmt.Println("Holi yo soy un número")
}

// las funciones reciben las variable como una copia del valor que venga en el tipo de dato
// estoy poniendo un * el cual es un puntero y lo uso ya que quiero modificar la informacion de la estructura del tipo de dato
// el metodo asignar edad para el tipo de dato Persona,
func (person *Persona) asignarEdad(edad int8) {
	if edad >= 0 {
		person.edad = edad
	} else {
		fmt.Println("No es valido la edad negativa")
	}
}

func main() {

	// un metodo es el comportamiento de un objeto,se puede asociar un metodo a cualquier tipo de dato
	var persona Persona
	// var numero Numero

	persona.saludar()
	// numero.presentarse()
	persona.asignarEdad(-21)
	fmt.Println(persona)
}
