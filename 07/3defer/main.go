package main

import (
	"fmt"
)

// nos permite ejecutar ciertas funciones despues de que termino una funcion que llamo esos defers
/* con defer es una sala de espera de cada función, lo que esta con defer se ejecuta al ultimo de mi función, se ejecuta del ultimo defer
al primer defer

*/

func main() {

	fmt.Println("Conectando a la base de datos...")
	defer cerrarDB()

	fmt.Println("Consultar información, set de datos")
	defer cerrarSetDeDatos()

}

func cerrarDB() {
	fmt.Println("Cerrar la Base de Datos")

}

func cerrarSetDeDatos() {
	fmt.Println("Cerrar el set de datos")

}
