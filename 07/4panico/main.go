package main

import (
	"fmt"
)

// a panic se le puede mandar cualquier cosa, en este caso le mandamos un string
func main() {
	// el panico muestra la fila de seguimiento de un problema inesperado
	// por ejemplo que alguine divida entre cero y eso no se puede y go entra en panico

	division(3, 0)

}

func division(dividendo, divisor int) {
	// panic deja que defer haga su trabajo pero solo cuando defer esta antes del panic *a excepcion que tenga un recover
	defer fmt.Println("Esto se ejecutrá pase lo que pase")

	if divisor == 0 {
		panic("No se puede dividir por cero")
	}

	fmt.Println(dividendo / divisor)

}
