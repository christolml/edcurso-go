package main

import (
	"fmt"
)

// el recover se ejecutan dentro de las funcioones defer, el recover es como nuestro respaldo cuando sucedan errores
// con recover podemos ejecutar como un plan b si pasa un error en el plan a
// un ejemplo sería la conexión a una base de datos, si por el primer método no me funciona y me manda un error con recover puedo
// que detectando ese error yo intente conectarme a una segunda base de datos

func main() {

	f()

}

func f() {
	// defer puede ejecutar una funcion anonima
	defer func() {
		// el if de abajo es cuando panic arroja algo
		// recover recupare lo arroja el panic y lo almacena en r
		// r := recover()    este esta dentro del if, si lo dejo como aqui funciona normal
		if r := recover(); r != nil {
			fmt.Printf("%T\n", r)
			fmt.Println("Recuperado en f: ", r)
		}
	}()
	// el ultimo fmt no se ejecuta ya que en g(5) se encontro un panic y se ejecuta el defer
	fmt.Println("Llamando g.")
	g(5)
	fmt.Println("Retorna normalmente desde g")
}

func g(i int) {
	// con el panic corta las demas instrucciones que estas despues del if, esto por el defer de la funcion f
	if i > 3 {
		fmt.Println("Entrando en panico aaaaaaaaaaaaaaa")
		panic("El número no puede ser mayor a 3")
	}

	defer fmt.Println("Defer en la función g")
	fmt.Println("Imprimiendo en g", i)

}
