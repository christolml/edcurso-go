package main

import (
	"github.com/christolml/edCurso-GO/07/2interfaces/animales"
)

func main() {

	var perr animales.Perro
	var gat animales.Gato

	perr.Nombre = "Trosca"
	gat.Nombre = "Towi"
	// adoptarPerro(perr)
	// adoptarGato(gat)
	adoptarMascota(perr)
	adoptarMascota(gat)
}

/*
func adoptarPerro(perr animales.Perro) {
	perr.Comunicarse()

}

func adoptarGato(gat animales.Gato) {
	gat.Comunicarse()
}
*/

// con la interfaz nos permite que todos mis archivos (perro,gato y main) se puedan comunicar
func adoptarMascota(interf animales.Mascota) {
	interf.Comunicarse()
}
