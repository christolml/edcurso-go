package animales

import (
	"fmt"
)

type Gato struct {
	Nombre string
}

func (gat Gato) Comunicarse() {
	fmt.Println("miauuuuu")
}
