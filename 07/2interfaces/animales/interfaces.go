package animales

// se crea una interfaz llamada mascota  el cual tiene un metodo comunicarse
// es un tipo de dato interface, lo que hará Mascota será comunicarse el cual es un metodo
// para que perro y gato puedan implementar esta interfaz lo unico que se tiene que hacer es que la estructura de perro y Gato
// y cualquier otra estrucutra que quiera implementar la interfaz deben de tener el metodo que esta en esta interfaz (Comunicarse()) se debe llamar igual
type Mascota interface {
	Comunicarse()
}

// para las interfaces el tipo de dato que recibe Comunicarse en los archivos de perro y gato no puede recibir en el tipo de dato un puntero
