package migration

// ESTE PAQUETE NOS VA A SERVIR PARA CREAR LOS MODELOS EN LA BASE DE DATOS

import (
	"edCurso-GO/10Proyecto/configuration"
	"edCurso-GO/10Proyecto/models"
)

// Migrate permite crear las tablas en la bd
func Migrate() {
	db := configuration.GetConnection()

	defer db.Close()

	db.CreateTable(&models.User{})
	db.CreateTable(&models.Comment{})
	db.CreateTable(&models.Vote{})
	db.Model(&models.Vote{}).AddUniqueIndex("comment_id_user_id_unique", "comment_id", "user_id")
}
