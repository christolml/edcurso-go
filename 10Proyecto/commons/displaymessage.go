package commons

import (
	"encoding/json"
	"edCurso-GO/10Proyecto/models"
	"net/http"
	"log"
)

// DisplayMessage devuelve un mensaje al cliente
func DisplayMessage(w http.ResponseWriter, m models.Message) {
	
	js, err := json.Marshal(m)
	if err != nil {
		log.Fatalf("Error al convertir el mensaje: %s", err)
	}
	
	w.WriteHeader(m.Code)
	w.Write(js)
	
}





