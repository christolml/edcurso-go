package commons

import (
	"crypto/rsa"
	"io/ioutil"
	"log"
	"github.com/dgrijalva/jwt-go"
	"edCurso-GO/10Proyecto/models"
)

// PublicKey se usa para validar el token
var (
	privateKey *rsa.PrivateKey
	PublicKey *rsa.PublicKey
)


func init() {
	privateBytes, err := ioutil.ReadFile("./keys/private.rsa")
	if err != nil {
		log.Fatal("No se pudo leer el archivo privado")
	}
	
	publicBytes, err := ioutil.ReadFile("./keys/public.rsa")
	if err != nil {
		log.Fatal("No se pudo leer el archivo publico")
	}
	
	privateKey, err = jwt.ParseRSAPrivateKeyFromPEM(privateBytes)
	if err != nil {
		log.Fatal("No se pudo hacer el parse a privateKey")
	}
	
	PublicKey, err  = jwt.ParseRSAPublicKeyFromPEM(publicBytes)
	if err != nil {
		log.Fatal("No se pudo hacer el parse a PublicKey")
	}
	
}

// GenerateJWT genera el token para el cliente, va a firmar el token
func GenerateJWT(user models.User) string {
	claims := models.Claim{
		user,
		jwt.StandardClaims{
			Issuer: "Company Christo",
		},
	}
	
	// se tiene el token listo, con SignedString se firma el token
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	result, err := token.SignedString(privateKey)
	if err != nil {
		log.Fatal("No se pudo firmar el token")
	}
	
	return  result
}


