package routes

import (
	"github.com/gorilla/mux"
	"edCurso-GO/10Proyecto/controllers"
)

// SetLoginRouter router para el login
func SetLoginRouter(router *mux.Router) {
	router.HandleFunc("/api/login", controllers.Login).Methods("POST")
	
}
