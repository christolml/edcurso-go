package routes

import "github.com/gorilla/mux"

// ESTE ARCHIVO ES EL QUE VA A TENER TODAS LAS RUTAS, SERA EL CENTRO QUE VA UNIR TODAS LAS RUTAS DE NUESTRO PAQUETE ROUTES

// InitRoutes inicia las rutas
func InitRoutes() *mux.Router {
	router := mux.NewRouter().StrictSlash(false)
	SetLoginRouter(router)
	SetUserRouter(router)
	SetCommentRouter(router)
	SetVoteRouter(router)
	SetRealtimeRouter(router)
	SetPublicRouter(router)
	
	return router
}
