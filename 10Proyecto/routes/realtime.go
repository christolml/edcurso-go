package routes

import (
	"github.com/gorilla/mux"
	"gopkg.in/olahol/melody.v1"
	"net/http"
)

// SetRealtimeRouter es la ruta para el realtime
func SetRealtimeRouter(router *mux.Router) {
	mel := melody.New()
	router.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		mel.HandleRequest(w,r)
		
	})
	mel.HandleMessage(func(s *melody.Session, msg []byte) {
		mel.Broadcast(msg)
	})
}
