package controllers

import (
	"net/http"
	"edCurso-GO/10Proyecto/models"
	"encoding/json"
	"fmt"
	"edCurso-GO/10Proyecto/configuration"
	"crypto/sha256"
	"edCurso-GO/10Proyecto/commons"
	"log"
	"crypto/md5"
)


// Login es el controlador de login
func Login (w http.ResponseWriter, r *http.Request) {
	user := models.User{}
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		fmt.Fprintf(w, "Error: %s",err)
		return
	}
	
	db := configuration.GetConnection()
	defer db.Close()
	
	c := sha256.Sum256([]byte(user.Password))
	pwd := fmt.Sprintf("%x",c)
	// pwd := base64.URLEncoding.EncodeToString(c[:32])
	
	db.Where("email = ? and password = ?",user.Email, pwd).First(&user)
	// si se llega a recibir un usuario, el id del usuario debera ser diferente a cero (el id puesto en la bd)
	log.Println(user.ID,  pwd)
	if user.ID > 0 {
		user.Password = ""
		token := commons.GenerateJWT(user)
		js, err := json.Marshal(models.Token{token})
		if err != nil {
			log.Fatalf("Error al convertir el token a json: %s",err)
		}
		
		w.WriteHeader(http.StatusOK)
		w.Write(js)
		
	} else {
		m := models.Message{
			Message:"Usuario o clave no valido",
			Code: http.StatusUnauthorized,
		}
		
		commons.DisplayMessage(w,m)
	}
	
}


// UserCreate permite registrar un usuario
func UserCreate(w http.ResponseWriter, r *http.Request) {
	
	user := models.User{}
	m := models.Message{}
	
	
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		m.Message = fmt.Sprintf("Error al leer el usuario a registrar: %s", err)
		m.Code = http.StatusBadRequest
		commons.DisplayMessage(w,m)
		return
	}
	
	if user.Password != user.ConfirmPassword {
		m.Message = "Las contraseñas no coinciden"
		m.Code = http.StatusBadRequest
		commons.DisplayMessage(w,m)
		return
	}
	
	c := sha256.Sum256([]byte(user.Password))
	pwd := fmt.Sprintf("%x",c)
	user.Password = pwd
	
	picmd5 := md5.Sum([]byte(user.Email))
	picstr := fmt.Sprintf("%x",picmd5)  //email codificado a md5
	user.Picture = "https://gravatar.com/avatar/" + picstr + "?s=100"  // gravatar devuelve una imagen
	
	db := configuration.GetConnection()
	defer db.Close()
	
	err = db.Create(&user).Error
	if err != nil {
		m.Message = fmt.Sprintf("Error al crear el registro: %s",err)
		m.Code = http.StatusBadRequest
		commons.DisplayMessage(w,m)
		return
	}
	
	
	m.Message = "Usuario creado con exito"
	m.Code = http.StatusCreated
	commons.DisplayMessage(w,m)
	
	
}







