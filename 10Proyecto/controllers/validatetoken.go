package controllers

import (
	"net/http"
	"edCurso-GO/10Proyecto/models"
	"github.com/dgrijalva/jwt-go/request"
	"github.com/dgrijalva/jwt-go"
	"edCurso-GO/10Proyecto/commons"
	"context"
)

/* ValidateToken permite validar el token del cliente
 el metodo para extraer informacion, OAuth2Extractor
 puntero a la estructura del token que se envia, &models.Claim{}
 funcion que devuelve la llave publica para validar si la firma del token es valida o no, devuelve la interface de la llave
*/
func ValidateToken(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	var m models.Message
	token, err := request.ParseFromRequestWithClaims(
		r,
		request.OAuth2Extractor,
		&models.Claim{},
		func(t *jwt.Token) (interface{}, error) {
			return commons.PublicKey, nil
		},
	)
	
	if err != nil {
		m.Code = http.StatusUnauthorized
		switch err.(type) {
		case *jwt.ValidationError:
			vError :=  err.(*jwt.ValidationError)
			switch vError.Errors {
			case jwt.ValidationErrorExpired:
				m.Message = "Su token ha expirado"
				commons.DisplayMessage(w,m)
				return
			case jwt.ValidationErrorSignatureInvalid:
				m.Message = "La firma del token no coincide"
				commons.DisplayMessage(w,m)
				return
			default:
				m.Message = "Su token no es valido"
				commons.DisplayMessage(w,m)
				return
				
			}
		
		}
	}
	
	if token.Valid {
		ctx := context.WithValue(r.Context(), "user",token.Claims.(*models.Claim).User)
		next(w,r.WithContext(ctx))
	} else {
		m.Code = http.StatusUnauthorized
		m.Message = "Su token no es valido"
		commons.DisplayMessage(w,m)
	}
	
	
}