package main

import (
	"flag"
	"log"

	"edCurso-GO/10Proyecto/migration"
	"edCurso-GO/10Proyecto/routes"
	"github.com/urfave/negroni"
	"net/http"
	"edCurso-GO/10Proyecto/commons"
	"fmt"
)

// migrate, almacena si la persona envio yes o no
func main() {
	var migrate string
	flag.StringVar(&migrate, "migrate", "no", "Genera la migracion a la base de datos")
	flag.IntVar(&commons.Port, "port", 8080, "Puerto para el servidor web")
	flag.Parse()
	if migrate == "yes" {
		log.Println("Comenzo la migracion...")
		migration.Migrate()
		log.Println("Finalizo la migracion.")
	}
	
	// Inicia las rutas
	router := routes.InitRoutes()
	
	// Inicia los middlewares
	n := negroni.Classic()
	n.UseHandler(router)
	
	// Inicia el servidor
	server := &http.Server{
		Addr: fmt.Sprintf(":%d", commons.Port), //":8080",
		Handler: n,
	}
	
	log.Printf("Iniciando el servidor en http://localhost:%d", commons.Port)
	log.Println(server.ListenAndServe())
	log.Println("Finalizo la ejecucion del programa")

}
