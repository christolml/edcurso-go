package models

import "github.com/jinzhu/gorm"

/*cuando se hace consultas especificas necesitamos saber el nombre del usuario pero llamandolo desde el comentario
no desde el usuario, cuando se consulta el usuario me trae los comentarios que tiene, cuando se hace una consulta al
comentario nos trae el id del usuario mas no toda la información de este, en este caso Declaramos una propiedad de User
para que cuando se consulte un comentario también se pueda consultar la información del usuario
Children => son comentarios respuestas al comentario original
*/

// Comment comntario del sistema
type Comment struct {
	gorm.Model
	UserID   uint      `json:"userId"`
	ParentID uint      `json:"parentId"`
	Votes    int32     `json:"votes"`
	Content  string    `json:"content"`
	HasVote  int8      `json:"hasVote" gorm:"-"`
	User     []User    `json:"user,omitempty"`
	Children []Comment `json:"children,omitempty"`
}
