package models

// Con este model nos permite tener la estructura de mensajes que le enviamos al cliente, mensajes como error, contenido creado, comentario agregado, etc

// Message mensaje para el cliente de la api
type Message struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
}
