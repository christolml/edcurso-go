package models

// el jwt antes del github.com......  es un alias para no estar escribiendo jwt-go
import "github.com/dgrijalva/jwt-go"

// Este token va a tener un User de la estructura user así que no se ocupa poner User User en go ya sabe que es del tipo User

// Claim Token de usuario
type Claim struct {
	User `json:"user"`
	jwt.StandardClaims
}
