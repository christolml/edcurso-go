package models

// Este model token nos va a servir para envolver la información obtenida en el model claim

// Token permite envolver el token generado
type Token struct {
	Token string `json:"token"`
}
