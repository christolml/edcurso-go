package models

import "github.com/jinzhu/gorm"

/* ESTE ARCHIVO ES EL MODELO DEL USUARIO
User empieza con mayuscula para exportarlo
gorm.Model => indica que va a hacer un modelo de gorm
Estamos haciendo una api por tanto la información la vamos a enviar y vamos a recibir del tipo json
ahí mismo en la anotación de json le indicamos a gorm ciertas condicionesÑ not null (no nulo) unique (unico) con gorm: "-" le indicamos
que no lo guarde en la base de datos
en password estamos indicando en la anotación de json el omitempty para decirle a json omite la propiedad si se encuentra vacio
*/

// User usuario del sistema
type User struct {
	gorm.Model
	Username        string    `json:"username" gorm:"not null;unique"`
	Email           string    `json:"email" gorm:"not null;unique"`
	Fullname        string    `json:"fullname" gorm:"not null"`
	Password        string    `json:"password,omitempty" gorm:"not null;type:varchar(256)"`
	ConfirmPassword string    `json:"confirmPassword,omitempty" gorm:"-"`
	Picture         string    `json:"picture"`
	Comments        []Comment `json:"comments,omitempty"`
}
