package main

import (
	"errors"
	"fmt"
)

func main() {

	/*
		los errores son un tipo de datos
		con el de abajo declaro mis errores
		err := errors.New("Mensaje de error")
		fmt.Printf("%T\n", err)
	*/

	// las variables resultado y err de abajo estan recibiendo lo que devuelve la funcion division
	resultado, err := division(100, 2)
	/*
		con el if de abajo me indica que mi error contiene algo ya que su valor por defecto es nil y cuando mi error no tiene el valor de nil entra al if de abajo y me muestra
		un mensaje de error y el error en si que me fue enviado por la funcion
	*/
	if err != nil {
		fmt.Println("Error", err)
		return
		// sin el return de arriba estaría ejecutando "fmt.Println(resultado, err)" y el resultado me devuelve un 0 ya que ese es el valor por defecto de float64
	}
	fmt.Println(resultado, err)

}

// esta funcion me regresa un float y un erorr
func division(dividendo, divisor float64) (resultado float64, err error) {

	if divisor == 0 {
		err = errors.New("No esta permitido dividir por 0")
	} else {
		resultado = dividendo / divisor
	}
	// con el return devuelve los valores de resultado y de err
	return

}
