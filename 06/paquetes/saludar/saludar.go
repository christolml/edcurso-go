package saludar

import (
	"fmt"
)

//MeVes es para utilizar desde otro paquete
var MeVes string

var noMeVes string

/*
con la S mayuscula significa que mi funcion sera una funcion a exportar
y este paquete lo puedo usar en otros paquetes
Saludar saluda personas
*/
func Saludar(nombre string) {
	fmt.Println("Holi, ", nombre)
}

func noVisible() {
	fmt.Println("No se ve desde otro paquete")
}
