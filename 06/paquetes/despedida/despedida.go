package despedida

import "fmt"

// se despide de una persona
func Despedirse(nombre string) {
	fmt.Println("Adios, ", nombre)
}
