package main


import (
	"fmt"
	"edCurso-GO/06/paquetes/despedida"
	"edCurso-GO/06/paquetes/saludar"
)


 func main() {
 	saludar.Saludar("Esmeralda")
 	saludar.MeVes = "Esto es un texto asignado desde el main"
 	fmt.Println(saludar.MeVes)

 	despedida.Despedirse("Christopher")

}
