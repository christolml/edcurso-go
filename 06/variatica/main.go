package main

import (
	"fmt"
)

func main() {
	saludarVarios(18, "Christopher", "Dana", "Esmeralda", "Trosca", "Paco")

}

/*
*una función variatica recibe un numero variable de parametros del mismo tipo
 *en esta función no se sabe cuantos nombres se va a pasar para saludar, por eso los tres puntos y el tipo de la variable, con los tres puntos
 estoy indicando que será un slice pero cuando sea llamada van a ser valores separados
 *cuenta con restricción de que en una función variatica solo se puede tener un parametro variatico y debe estar al final de los parametros que la
 función requiera
*/
func saludarVarios(edad uint8, nombres ...string) {
	// con el de abajo me indica de que tipo es mi variable nombres y me imprime que es un slice de string
	fmt.Printf("%T\n", nombres)
	for _, v := range nombres {
		fmt.Println("Holi,", v, "tienes", edad, "años de edad")
	}

}
