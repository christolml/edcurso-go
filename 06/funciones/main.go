package main

import (
	"fmt"
)

// sintaxis normal de una funcion
func main() {
	saludar("Christopher", 21)
}

func saludar(nombre string, edad uint8) {
	fmt.Printf("Hi %s you have %d years old\n", nombre, edad) //se le da formato y ya es mas practico pasar mis variables
	// fmt.Println("Hello", nombre, "you have", edad, "years old")
	if edad > 30 {
		return
	}
	fmt.Println("Eres joven")
}
