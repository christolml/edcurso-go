package main

import (
	"fmt"
)

func main() {

	/* funcion anonima auto ejecutandose
	func() {
		fmt.Println("me imprimo")
	}()
	*/

	// funcion anonima asignada a una variable
	/*
		anonima := func() {
			fmt.Println("me imprimo desde una variable llamada anonima")
		}
	*/

	// anonima()
	// me imprime el tipo de dato que es mi variable anonima y me dice que su tipo es una func
	// fmt.Printf("%T\n", anonima)

	secuencia1 := secuencia()
	fmt.Println(secuencia1())
	fmt.Println(secuencia1())
	fmt.Println(secuencia1())
	fmt.Println(secuencia1())
	fmt.Println("---------------")
	secuencia2 := secuencia()
	fmt.Println(secuencia2())
	fmt.Println(secuencia2())

}

// esta funcion regresa una funcion y a su vez esta segunda funcion regresa un entero32
func secuencia() func() int32 {
	var x int32

	return func() int32 {
		x++
		return x
		// en GO no se puede hacer return x++ ya que en go eso no lo considera una expresion
		// sino una instruccion, en otros lenguajes si se puede
	}
}
