package main

import (
	"fmt"
)

func main() {
	// un puntero es la direccion en memoria a una variable de un tipo determinado
	// el valor cero de un puntero es nil

	a := 3
	// fmt.Println(&a)
	fmt.Println("Antes de duplicar , a= ", a)
	duplicar(&a) // para acceder a la direccion de memoria prepongo el & a mi variable
	fmt.Println("Despues de duplicar, a= ", a)

}

// a esta funcion le estoy pasando el puntero de la variable
// recibe un puntero a un entero
func duplicar(a *int) {
	// para acceder al valor de una direccion de memoria tengo que poner * a mi variable
	*a *= 2
	fmt.Println("Dentro de la funcion duplicar a=  ", *a)
}
