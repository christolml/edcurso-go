package main

import (
	"fmt"
)

func main() {
	var n1, n2 int8
	n1 = 15
	n2 = 20
	r := suma(n1, n2)

	fmt.Println(r)

	var edad2 uint8
	edad2 = 7
	fmt.Println(tipoEdad(edad2))

	n := []int8{4, 7, 6, 10, 12, 27, 30, 11, 5, -7, 34}
	maximo, minimo := maxymin(n)
	fmt.Println("Máximo: ", maximo, "\n", "Mínimo: ", minimo)

}

/* esta funcion me regresa como resultado un int8 por eso antes
de las llaves declaro el tipo de dato que se va aregresar   */
func suma(a, b int8) int8 {
	return a + b
}

// el tipo de salida de una funcion no es necesario que sea igual al tipo que entra en los parametros
func tipoEdad(edad uint8) string {
	var tipo string
	switch {
	case edad < 12:
		tipo = "Niñez"
	case edad < 18:
		tipo = "Adolescencia"
	default:
		tipo = "Madurez"
	}

	return tipo
}

// ---------------------RETORNO DE MULTIPLES VALORES
/* como esta función va a regresar mas de un valor se abre parentesis y se pone el tipo de datos que serán los valores devueltos
en el return ya no es necesario especificar las variables ya que desde la firma de la funcion
 estoy declarando en cero las variables a usar en la funcion y cuando se ejecute toda la funcion y se encuentre un return lo que hará este
 será devolvernos el valor de nuestras variables que tenemos como parametros en nuestra firma de la función
*/
func maxymin(numeros []int8) (max int8, min int8) {
	for _, v := range numeros {
		if v > max {
			max = v
		}
		if v < min {
			min = v
		}
	}
	return
	// return max, min
}
