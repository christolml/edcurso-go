package main

import (
	"html/template"
	"log"
	"net/http"
)

// Persona contiene los datos de una persona
type Persona struct {
	Nombre string
	Edad   uint8
}

// se esta creando este handleFunc el cual procesa un archivo html con los datos de una persona
func renderizar(w http.ResponseWriter, r *http.Request) {
	// esta variable tiene un apuntador a la estructura persona
	p := &Persona{"Christopher", 21}
	/*	ParseFiles analiza un archivo html, recibe la ruta del archivo a analizar
		Parsefiles devuelve el template y un error*/
	t, err := template.ParseFiles("./views/index.html")
	if err != nil {
		/*este error es error del servidor, usamos el paquete http con su funcion Error, err.Error() le envia el error al navegador
		el ultimo parametro es el codigo http del error se la funcion StatusInternalServerError devuelve mas o menos que error sucedio */
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	/*	le estamos diciendo al template que se ejecute, ocupa el writer y los datos que el template necesita, en este caso
		como nuestra html ya tiene algo no le pasamos nada y ponemos nil*/
	t.Execute(w, p)

}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", renderizar)

	log.Println("Ejecutando server en http://localhost:8080")
	log.Println(http.ListenAndServe(":8080", mux))
	log.Println("Ejecucion terminada, ya no esta corriendo el servidor")

}
