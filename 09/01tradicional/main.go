package main

import (
	"log"
	"net/http"
)

func main() {

	/* en este handle le estoy pasando la raiz de nuestro servidor web, cuando se accede a ese raiz le vamos a servir la carpeta public
	le estamos dando una raiz root cuando se acceda al servidor web y se le esta diciendo al servidor que es lo que tiene que hacer  */
	// http.Handle("/", http.FileServer(http.Dir("public")))

	// log.Println("Ejecutando server en http://localhost:8080")

	/*
		se esta cargando el servidor y se especifica en que puerto va a trabajar, luego se pide el mux que va a servir, como se esta usando la funcion handle y se le esta
		diciendo a que ruta se ha de servir ("/") no se pone nada solamente nil */
	// log.Println(http.ListenAndServe(":8080", nil))

	/* -------------------------MULTIPLEXOR
	El multiplexor (MUX) es un circuito combinacional que tiene varios canales de datos de entrada y
	solamente un canal de salida. Sólo un canal de la entrada pasará a la salida y este será el que haya
	sido escogido mediante unas señales de control.

	Es una herramienta que permite recibir varias solicitudes al mismo tiempo y procesarlas
	*/

	// Creando un multiplexor interno de http
	mux := http.NewServeMux()
	// Se esta sirviendo la carpeta (archivos) que quiero mostrar en la web
	fs := http.FileServer(http.Dir("public"))

	// se esta haciendo un manejador, el primer parametro es la ruta, el segundo parametro es lo queremos que sirva
	mux.Handle("/", fs)
	log.Println("Ejecutando server en http://localhost:8080")
	// en ListenAndServe ya se esta pasando el multiplexor el mux, el mux ya tiene los controladores que va a manejar de acuerdo a la ruta, en la ruta "/" va a servir esos archivos estaticos
	log.Println(http.ListenAndServe(":8080", mux))

}
