package main

import (
	"fmt"
	"log"
	"net/http"
)

// los handler func es un tipo de dato que permite convertir funciones en manejadores http
//  requsitos, el nombre de la funcion con parametros de response writer y el request
// en este ejercicio se pone en practica como manejar todoo lo que tiene que ver con las rutas y sus respectivos controladores

/* handlerfunc normal que luego se pasa como parametro a mi mux.handleFunc
es un handler que imprime un mensaje a la pag web*/
func messageHandlerFunc(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Holi esto es un handlerFunc</h1>")

}

/* handler func con mensaje personalizado, esta funcion devuelve un handlefunc
el handlerfunc que estoy retornando ocupa una funcion que va a retornar
la funcion de abajo lo que hace es recibir un mensaje personalizado y devuelve un handlerfunc
el cual nos imprime el mensaje personalizado*/
func messageHFCustom(message string) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, message)
		},
	)

}

/* se usa HandleFunc cuando el handler tiene la firma de w http.responsewriter, r *http.request
y se usa handle cuando la firma de la funcion no tiene w y r */
func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", messageHandlerFunc) //a mi multiplexor le estoy asignando el controlador que cree
	mux.Handle("/saludar", messageHFCustom("<h1>Holi desde un msj personalizado</h1>"))
	mux.Handle("/despedirse", messageHFCustom("<h1>Hasta la proxima amigos lml</h1>"))

	log.Println("Ejecutando server http://localhost:8080")
	log.Println(http.ListenAndServe(":8080", mux))

}
