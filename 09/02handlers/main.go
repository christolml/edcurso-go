package main

import (
	"fmt"
	"log"
	"net/http"
)

/*
ESTA LINEA los handler son los manejadores para las rutas que nosotros diseñemos en la aplicacion IMPORTANTE
un handler es una interfaz que contiene un metodo que nos permite recibir como parametro un responsewriter el cual es la interfaz que nos permite escribir o devolverle inf al usuario
y un request que es la solicitud del usuario
*/

type messageHandler struct {
	message string
}

/*
implementa una interfaz en go, este es un metodo que ResponseWriter la herramienta con la cual vamos a devolver la información al usuario
y el request que es la petición del usuario
*/
func (m messageHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Fprintf, nos permite usar un writer para enviar informacion, el primer parametro es para indicarle a que writer va a escribir y el segundo es lo que se quiere escribir
	fmt.Fprintf(w, m.message)
}

func main() {
	mux := http.NewServeMux()
	m1 := &messageHandler{message: "<h1> Holi desde un handler</h1>"}
	m2 := &messageHandler{message: "<h1>Estos son perritos lml</h1>"}
	m3 := &messageHandler{message: "<h1>Estos son gatitios :(</h1>"}

	mux.Handle("/saludar", m1)
	mux.Handle("/perros", m2)
	mux.Handle("/gatos", m3)
	log.Println("Ejecutando server en http://localhost:8080")
	log.Println(http.ListenAndServe(":8080", mux))

}
