package main

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

// Producto contiene los datos de un articulo
type Producto struct {
	gorm.Model   // ID, CreateAt, UpdateAt, DeleteAt
	CodigoBarras string
	Precio       uint
}

func main() {
	db, err := gorm.Open("mysql", "root:@/gorm?charset=utf8&&parseTime=True&loc=Local")
	if err != nil {
		panic("Error a la conexión a la base de datos")
	}

	defer db.Close()
	fmt.Println("Se conecto a la base de datos")

	/*	db.CreateTable(&Producto{})
		fmt.Println("Se creo la tabla productos en la base de datos")*/

	/*	crear un registro en la base de datos, en la tabla productos
		los nombres en las tablas son en plural asi lo trabaja gorm
		db.Create(&Producto{CodigoBarras:"esteesuncodigobarra", Precio:5000}) */

	// Consultando los productos en la base de datos
	var p Producto
	/*consulta la informacion del primer producto y lo almacena en p
	el segundo parametro de First es el where donde se le pasa el id del producto a consultar*/
	db.First(&p, 2)
	fmt.Println(p)
	fmt.Println("Precio del producto consultado:", p.Precio)

	// modificamos el precio de la base de datos y lo guardamos con Save
	p.Precio = 2700
	db.Save(&p)
	fmt.Println("El nuevo precio del producto es:", p.Precio)

}
